An utility to retrieve the Gitlab CI templates used by projects.

* As a project tech leader, check the consistency of templates used by all your sub-projects
* As a Gitlab administrator, evaluate the usage of templates on the whole platform

# Download

The precompiled binaries are available in [release page](https://gitlab.com/gbonnefille/template-crawler/-/releases/).

# Usage

1. Create a token on the targeted Gitlab with permissions `api_read` and `repository_read`.
2. Run the command specifying the URL of the Gitlab and the token:
      `template-crawler --base https://gitlab.example.com --token 12ab34cd`

The output will look like:

    ...
    The project to-be-continuous/docker uses the template to-be-continuous/kicker in version master
    The project to-be-continuous/docker uses the template to-be-continuous/bash in version 3.1.1
    The project to-be-continuous/docker uses the template to-be-continuous/semantic-release in version 3.2.0
    ...

## Synopsis

```
An utility to retrieve the Gitlab CI templates used by projects.

Usage:
  template-crawler [flags]

Flags:
  -b, --base string          Base URL (default "https://gitlab.com")
      --date-format string   Format of date (golang date's format specification, see https://pkg.go.dev/time#example-Time.Format) (default "2006-01-02T15:04:05-07:00")
      --debug                activate debug messages
  -f, --format string        Format (text or csv or prom) (default "text")
  -g, --group stringArray    Groups names
  -h, --help                 help for template-crawler
  -p, --password string      Password
      --since string         Lower date limit to retain data based on last pipeline date: can be a duration from now (10h, -1m) or instant time following --date-format
  -t, --token string         Token
      --until string         Upper date limit to retain data based on last pipeline date: can be a duration from now (10h, -1m) or instant time following --date-format
  -u, --username string      Username
      --verbose              make the output more verbose
```

## Format

Multiple output formats supported.

### Text

Example of Text output, with `--format text`:

    ...
    The project to-be-continuous/docker uses the template to-be-continuous/kicker in version master
    The project to-be-continuous/docker uses the template to-be-continuous/bash in version 3.1.1
    The project to-be-continuous/docker uses the template to-be-continuous/semantic-release in version 3.2.0
    ...

### CSV

Example of CSV output, with `--format csv`:

    project,template file,template project,template ref
    to-be-continuous/tools/tracking,/templates/gitlab-ci-docker.yml,to-be-continuous/docker,3.5.0
    to-be-continuous/tools/tracking,/templates/gitlab-ci-golang.yml,to-be-continuous/golang,4.0.0
    to-be-continuous/tools/vault-secrets-provider,templates/gitlab-ci-golang.yml,to-be-continuous/golang,4.0.0
    to-be-continuous/tools/vault-secrets-provider,templates/gitlab-ci-docker.yml,to-be-continuous/docker,3.5.0
    ...

### Prometheus

Example of Prometheus metrics output, with `--format prom`:

    ...
    gitlab_template{project="to-be-continuous/tools/gitlab-ci",file="/templates/validation.yml",ref="master"} 33
    gitlab_template{project="to-be-continuous/kicker",file="/templates/validation.yml",ref="master"} 33
    gitlab_template{project="to-be-continuous/maven",file="templates/gitlab-ci-maven.yml",ref="3.1.2"} 1
    ...

This export can be published via a Prometheus pushgateway.
See Examples below for a detailed example.

## Date selection

By default, all usage of template are reported.
But it is possible to filter data based on the date of the last pipeline run.

The `--since` is the lower limit and `--until` is the upper limit.
They both can be specified as a duration from now (10h, -1m) or instant time if specified following the layout set via `--date-format`.

# Examples

## All projects

It is possible to crawl all projects accessible from a given token, but limited to fresh usage from the last year:

```
template-crawler -t $TOKEN -b https://gitlab.com --since -8760h
```

An administrator is able to check the usage of template in the whole platform.
A DevOps Angel is able to keep an eye on all the projects he participated on.

## Given group

It is possible to crawl only a selection of groups:

```
template-crawler -t $TOKEN -b https://gitlab.com --group to-be-continuous/samples --group my-own-group
```

A project manager is then able to check the consistency of templates in his groups.

## Metrics for many instances

Here, we have one hosted Gitlab instance and some projects on the Gitlab SaaS.
Regularly (weekly?) we run:

```
template-crawler -t $TOKEN_CORP -b https://gitlab.example.com -f prom | curl -s --data-binary @- http://pushgateway.example.com/metrics/job/tbc/instance/gitlab.example.com
template-crawler -t $TOKEN_SAAS -b https://gitlab.com --group mycorp -f prom | curl -s --data-binary @- http://pushgateway.example.com/metrics/job/tbc/instance/gitlab.com
```

We are then able to track the effective usage of the different templates across our projects.