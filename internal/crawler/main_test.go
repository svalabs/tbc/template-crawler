package crawler

import (
	"testing"
)

func TestProjectInclude(t *testing.T) {
	data := `
include:
  - project: 'to-be-continuous/golang'
    ref: '4.1.0'
    file: '/templates/gitlab-ci-golang.yml'
`
	gc := ParseGitlabCiYaml([]byte(data))
	if len(gc.GetProjectInclude()) != 1 {
		t.Errorf("Expecting 1 got %d", len(gc.GetProjectInclude()))
	}
}

// Single include
func TestLocalInclude(t *testing.T) {
	data := `
include: '/gitlab-ci-golang.yml'
`
	gc := ParseGitlabCiYaml([]byte(data))
	if len(gc.GetProjectInclude()) != 0 {
		t.Errorf("Expecting 0 got %d", len(gc.GetProjectInclude()))
	}
}

// Multiple includes not project
func TestIncludes(t *testing.T) {
	data := `
include:
- remote: 'https://gitlab.com/awesome-project/raw/main/.before-script-template.yml'
- local: '/templates/.after-script-template.yml'
- template: Auto-DevOps.gitlab-ci.yml
`
	gc := ParseGitlabCiYaml([]byte(data))
	if len(gc.GetProjectInclude()) != 0 {
		t.Errorf("Expecting 0 got %d: %v", len(gc.GetProjectInclude()), gc.Includes)
	}
}
