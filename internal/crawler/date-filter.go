package crawler

import (
	"fmt"
	"tbc/template-crawler/internal/logging"
	"time"
)

type DateFilter struct {
	Since *time.Time
	Until *time.Time
}

func (d DateFilter) Match(t *time.Time) bool {
	if d.Since != nil && t.Before(*d.Since) {
		return false
	}
	if d.Until != nil && t.After(*d.Until) {
		return false
	}
	return true
}

func (d DateFilter) String() string {
	return fmt.Sprintf("[ %s,  %s ]", d.Since, d.Until)
}

func parseDateFilterBound(layout string, spec string) *time.Time {
	if spec == "" {
		// Empty
		return nil
	}
	// Duration
	duration, err := time.ParseDuration(spec)
	if err == nil {
		t := time.Now().Add(duration)
		return &t
	}
	logging.Debug().Printf("Unable to parse duration %s: %v", spec, err)
	// Time
	t, err := time.Parse(layout, spec)
	if err == nil {
		return &t
	}
	logging.Debug().Printf("Unable to parse instant time %s: %v", spec, err)
	// Default
	return nil
}

// Parse two spec and build a DateFilter.
// First try to decode a duration and then use layout (standard Golang time packe layout) to parse a date.
func ParseDateFilter(layout string, sinceSpec string, untilSpec string) DateFilter {
	var since *time.Time
	var until *time.Time
	// Since
	since = parseDateFilterBound(layout, sinceSpec)
	// Until
	until = parseDateFilterBound(layout, untilSpec)
	// Filter
	return DateFilter{Since: since, Until: until}
}
