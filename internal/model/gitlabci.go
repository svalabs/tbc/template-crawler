package model

import "tbc/template-crawler/internal/logging"

type Include struct {
	Project string `yaml:"project"`
	Ref     string `yaml:"ref"`
	File    string `yaml:"file"`
}
type GitlabCI struct {
	Includes []interface{} `yaml:"include"`
}

func (p *GitlabCI) GetProjectInclude() []Include {
	res := make([]Include, 0)
	for _, raw := range p.Includes {
		st, ok := raw.(map[string]interface{})
		if ok {
			if _, ok := st["project"]; ok {
				project, ok := st["project"].(string)
				if !ok {
					logging.Debug().Printf("Failed to convert project: value as string: %v", st["project"])
					continue
				}
				file, ok := st["file"].(string)
				if !ok {
					logging.Debug().Printf("Failed to convert file: value as string: %v", st["file"])
					continue
				}
				ref, ok := st["ref"].(string)
				if !ok {
					// ref: is optional
					// https://docs.gitlab.com/ee/ci/yaml/#includeproject
					ref = "HEAD"
				}
				inc := Include{
					Project: project,
					Ref:     ref,
					File:    file,
				}
				res = append(res, inc)
			}
		}
	}
	return res
}
