package render

import (
	"fmt"
	"tbc/template-crawler/internal/model"
	"time"

	"github.com/xanzy/go-gitlab"
)

type PrometheusRenderer struct {
	byTemplate map[string]map[string]map[string]int
}

func NewPrometheusRenderer() PrometheusRenderer {
	return PrometheusRenderer{byTemplate: make(map[string]map[string]map[string]int)}
}

func (r PrometheusRenderer) Start() {
	// Nothing
}

func (r PrometheusRenderer) Add(proj *gitlab.Project, incl *model.Include, d *time.Time) {
	if _, ok := r.byTemplate[incl.Project]; !ok {
		r.byTemplate[incl.Project] = make(map[string]map[string]int)
	}
	if _, ok := r.byTemplate[incl.Project][incl.File]; !ok {
		r.byTemplate[incl.Project][incl.File] = make(map[string]int)
	}
	r.byTemplate[incl.Project][incl.File][incl.Ref] += 1
}

func (r PrometheusRenderer) Stop() {
	for proj, projM := range r.byTemplate {
		for file, fileM := range projM {
			for ref, count := range fileM {
				fmt.Printf(`gitlab_template_usage_total{project="%s",file="%s",ref="%s"} %d`, proj, file, ref, count)
				fmt.Println()
			}
		}
	}
}
