package render

import (
	"encoding/csv"
	"os"
	"tbc/template-crawler/internal/logging"
	"tbc/template-crawler/internal/model"
	"time"

	"github.com/xanzy/go-gitlab"
)

type CsvRenderer struct {
	writer     *csv.Writer
	dateFormat string
}

func NewCsvRenderer(dateFormat string) CsvRenderer {
	return CsvRenderer{
		writer:     csv.NewWriter(os.Stdout),
		dateFormat: dateFormat,
	}
}

func (r CsvRenderer) Start() {
	record := []string{"project", "template file", "template project", "template ref"}
	err := r.writer.Write(record)
	if err != nil {
		logging.Error().Fatalf("Failed to write header: %s", err)
	}
}

func (r CsvRenderer) Add(proj *gitlab.Project, incl *model.Include, date *time.Time) {
	d := ""
	if date != nil {
		d = date.Local().Format(r.dateFormat)
	}
	record := []string{proj.PathWithNamespace, incl.File, incl.Project, incl.Ref, d}
	err := r.writer.Write(record)
	if err != nil {
		logging.Error().Fatalf("Failed to write record: %s", err)
	}
}

func (r CsvRenderer) Stop() {
	r.writer.Flush()
}
